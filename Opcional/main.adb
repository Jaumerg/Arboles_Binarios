with Ada.Command_Line; use Ada.Command_Line;
with Ada.Text_IO; use Ada.Text_IO;
with Ada.Integer_Text_IO; use Ada.Integer_Text_IO;

with d_binary_tree;

procedure main is

	package intd_binary_tree is new d_binary_tree (integer,"+","-",integer'image);
	use intd_binary_tree;

	mytree : tree;
	file : File_Type;
	read : integer;
	found : boolean;

	-- Create the tree with a file
	procedure create_tree (t : out tree; file : in file_type) is
	begin
		while not end_of_file(file) loop
			get(file, read);
			insert(t, read);
		end loop;
	end create_tree;

	-- Write 1 in a file if the boolean variable introduced by parametrs is true
	-- if is false write a 0 in the file
	procedure put_result_on_file (file: in out file_type; bol : in boolean) is
	begin
		if bol then
			put(file,"1");
		else
			put(file,"0");
		end if;
	end put_result_on_file;



begin

	if Argument_Count < 2 then
		Put_Line("No se ha introducido el nombre del fichero");
	else

		empty(mytree);
		open(file, mode => in_file, name => Argument(1));
		create_tree(mytree, file);
		close(file);

		found := has_path_sum(mytree,integer'value(Argument(2)));
		create(file, mode => out_file, name => "resultados.txt");
		put_result_on_file(file, found);
		close(file);

	end if;

end main;

