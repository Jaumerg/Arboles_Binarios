with ada.text_io; use ada.text_io;

generic
	type key is (<>);
	with function "+"(a,b: in key) return key;
	with function "-"(a,b: in key) return key;
	with function image(k:in key) return string;
package d_binary_tree is

	type tree is limited private;

	bad_use : exception;
	space_overflow : exception;

	-- Prepare the free tree to start the operations
	procedure empty (t: out tree);
	pragma inline (empty);

	-- Insert new element in the tree following the width path
	procedure insert (t : in out tree; k : in key);

	-- Return true if the tree has a path that sum is equals than second 
	-- parameter
	function has_path_sum (t : in tree; i : in key) return boolean;

	-- Return a boolean depending on the root content
	function is_empty (t : in tree) return boolean;
	pragma inline (is_empty);

	-- Use the preorder path to print the tree
	procedure print_t (t : in tree);

private
	type node;
	type pnode is access node;

	type node is
		record 
			k: key;
			lc,rc: pnode;
		end record;

	type tree is
		record
			root: pnode;
		end record;

	-- Structure queue needed to create the binary tree with width path
	type cell;
	type pcell is access cell;

	type cell is
		record
			p : pnode;
			next : pcell;
		end record;

	type queue is
		record
			first, last : pcell;
		end record;

end d_binary_tree;
