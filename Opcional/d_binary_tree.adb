package body d_binary_tree is 

	myqueue : queue;

	procedure empty (t: out tree) is
	begin
		t.root := null;
	end empty;

	function is_empty (t : in tree) return boolean is
	begin
		return t.root=null;
	end is_empty;

	-- check the existence of the left child
	function exist_left_child(p : in pnode) return boolean is
	begin
		return p.lc/=null;
	end exist_left_child;
	pragma inline (exist_left_child);

	-- check the existence of the right child
	function exist_right_child(p : in pnode) return boolean is
	begin
		return p.rc/=null;
	end exist_right_child;
	pragma inline (exist_right_child);

	-- Code related to internal queue structure
	
	-- Operation that put the queue to empty
	procedure empty (myqueue : out queue) is
	begin
		myqueue.first := null;
		myqueue.last := null;
	end empty;
	pragma inline (empty);

	-- Check if the queue is empty
	function is_empty (myqueue : in queue) return boolean is
	begin 
		return myqueue.first = null;
	end is_empty;
	pragma inline (is_empty);

	-- Operation to insert an element in the queue structure
	procedure qinsert (myqueue : in out queue; p : in pnode) is
		newcell : pcell;
	begin
		newcell := new cell;
		newcell.all := (p, null);
		if is_empty(myqueue) then
			myqueue.first := newcell;
			myqueue.last := newcell;
		else
			myqueue.last.next := newcell;
			myqueue.last := newcell;
		end if;
	exception
		when storage_error => raise space_overflow;
	end qinsert;

	-- Operation to remove the first element in queue structure
	procedure rm_first (myqueue : in out queue) is
	begin
		myqueue.first := myqueue.first.next;
		if myqueue.first = null then myqueue.last := null; end if;
	exception
		when constraint_error => raise bad_use;
	end rm_first;

	-- Get the element at first position on the queue
	function get_first (myqueue : in queue) return pnode is
	begin
		return myqueue.first.p;
	exception
		when constraint_error => raise bad_use;
	end get_first;
	pragma inline (get_first);

	-- End of code related to private queue structure

	-- Insert new key in the tree
	procedure insert (t : in out tree; k: in key) is
		p : pnode;
	begin
		if is_empty(t) then
			t.root := new node'(k, null, null);
			empty(myqueue);
			qinsert(myqueue, t.root);
		else
			p := get_first(myqueue);
			if not exist_left_child(p) then
				p.lc := new node'(k, null, null);
				qinsert(myqueue, p.lc);
			else
				p.rc := new node'(k, null, null);
				qinsert(myqueue, p.rc);
				rm_first(myqueue);
			end if;
		end if;
	exception
		when storage_error => raise space_overflow;
	end insert;

	-- Check if exist any child
	function has_childs (p : in pnode) return boolean is
	begin
		return exist_right_child(p) or exist_left_child(p);
	end has_childs;
	pragma inline (has_childs);

	-- Use the preorder path to check if exist any path with specificated sum
	function preorder (p : in pnode; i : in key; sum : in key) 
															return boolean is
		s : key;
		ok : boolean := false;
	begin
		if not has_childs(p) then
			s := sum + p.k;
			return s = i;
		else
			s := sum + p.k;
			if exist_left_child(p) then
				ok := preorder(p.lc, i, s);
				if exist_right_child(p) and ok = false then
					ok := preorder(p.rc, i, s);
				end if;
			end if;
			return ok;
		end if;
	end preorder;

	function has_path_sum (t : in tree; i : in key) return boolean is
	begin
		return preorder(t.root, i, i-i);
	end has_path_sum;

	procedure print_t (p: in pnode) is
	begin
		if p=null then
			put(" - ");
		else
			put("( " & image(p.k));
			print_t (p.lc);
			print_t (p.rc);
			put (" )");
		end if;
	end print_t;

	procedure print_t (t : in tree) is
	begin
		print_t(t.root);
	end print_t;
end d_binary_tree;
