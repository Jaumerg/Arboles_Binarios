package body d_binary_tree is 

	type akey is array (key) of boolean;

	procedure empty (t: out tree) is
	begin
		t.root := null;
	end empty;

	-- Set as visited in the array one key both introduced by arguments
	procedure set_visited(visited: in out akey; k : in key) is
	begin
		visited(k) := true;
	end set_visited;
	pragma inline (set_visited);

	-- Check if one key was visited
	function is_visited(visited : in akey; k : in key) return boolean is
	begin
		return visited(k);
	end is_visited;
	pragma inline (is_visited);

	-- Contruct the tree with the inorder list, preorder queue and the visited
	-- akey type. This construction is recursive.
	procedure construct_tree_rec(actnode: in out pnode; lin : in out list;
							qpre : in out queue; visited : in out akey) is
	begin
		set_visited(visited, actnode.k);
		
		if not current_is_empty(lin) and then 
								not is_visited(visited, current_item(lin)) then
			-- Construct left child
			rm_first(qpre);
			if not is_empty(qpre) then
				actnode.lc := new node'(get_first(qpre), null, null);
				construct_tree_rec(actnode.lc, lin, qpre, visited);
			end if;
		end if;

		if not current_is_empty(lin) and then 
									is_visited(visited, current_item(lin)) then
			advance_pointer(lin);
			if not current_is_empty(lin) and then not is_empty(qpre) and then
								not is_visited(visited, current_item(lin)) then
				-- Construct right child
				rm_first(qpre);
				if not is_empty(qpre) then
					actnode.rc := new node'(get_first(qpre), null, null);
					construct_tree_rec(actnode.rc, lin, qpre, visited);
				end if;
			end if;
		end if;
	exception
		when storage_error => raise space_overflow;
	end construct_tree_rec;

	-- public procedure called to construct a tree with an inorder and 
	-- preorder queues
	procedure construct_tree_by_paths (t: in out tree; lin : in out list;
														qpre: in out queue) is
		visited : akey := (others => false);
	begin
		-- Inicializate the structures to empty
		if not is_empty(qpre) and then not current_is_empty(lin) then
			t.root := new node'(get_first(qpre), null, null);
			construct_tree_rec(t.root, lin, qpre, visited);
		end if;

		reset_pointer(lin);
	exception
		when storage_error => raise space_overflow;
	end construct_tree_by_paths;

	function is_empty (t : in tree) return boolean is
	begin
		return t.root=null;
	end is_empty;

	-- check the existence of the left child
	function exist_left_child(p : in pnode) return boolean is

	begin
		return p.lc/=null;
	end exist_left_child;
	pragma inline (exist_left_child);

	-- check the existence of the right child
	function exist_right_child(p : in pnode) return boolean is

	begin
		return p.rc/=null;
	end exist_right_child;
	pragma inline(exist_right_child);

	-- check if the the subtree is correct
	procedure check_tree0 (p : in pnode; lin : in out list; c : in out boolean)
																			  is
	begin
		-- visit left child
		if c and exist_left_child(p) then check_tree0(p.lc,lin,c); end if;
		-- visit node
		if c and not current_is_empty(lin) and p.k=current_item(lin) then
			advance_pointer(lin);
		else
			c := false;
		end if;
		-- visit right child
		if c and exist_right_child(p) then check_tree0(p.rc,lin,c); end if;
	end check_tree0;

	-- public procedure called by user to check if the tree is correct
	procedure check_tree (t : in tree; lin : in out list; c : in out boolean) is
	begin
		c := true;
		check_tree0 (t.root, lin, c);

		if not current_is_empty(lin) then
			c := false;
		end if;
		reset_pointer(lin);
	end check_tree;

	-- check if the value of the node is bigger than his left child
	function father_greater (p : in pnode) return boolean is
	begin
		return p.k > p.lc.k;
	end father_greater;
	pragma inline (father_greater);

	-- check if the value of the node is lower than his right child
	function father_lower (p : in pnode) return boolean is
	begin
		return p.k < p.rc.k;
	end father_lower;
	pragma inline (father_lower);

	-- check if the introduced key is in a range passed by arguments
	function in_range (x, min, max : in key) return boolean is
	begin
		return min <= x and x <= max;
	end in_range;
	pragma inline (in_range);

	-- return the predecessor key of the key introduced by arguments
	function predecessor (k : in key) return key is
	begin
		return key'pred(k);
	end predecessor;
	pragma inline (predecessor);

	-- return the successor key of the key introduced by arguments
	function successor(k : in key) return key is
	begin
		return key'succ(k);
	end successor;
	pragma inline (successor);

	-- Recursive procedure to check if the tree is an BST 
	procedure is_ACB0 (p : in pnode; c : in out boolean; min, max : in key) is
		nmin, nmax : key;
	begin
		if c and then exist_left_child(p) then
			if father_greater(p) and then in_range(p.lc.k, min, max) then 
				-- update range
				nmax := predecessor(p.k);

				is_ACB0(p.lc, c, min, nmax);

			else
				c:=false;
			end if;
		end if;
		
		if c and then exist_right_child(p) then
			if father_lower(p) and then in_range(p.rc.k, min, max) then 
				--update range
				nmin := successor(p.k);

				is_ACB0(p.rc,c, nmin, max);

			else
				c := false;
			end if;
		end if;

	end is_ACB0;

	-- check if the tree is a BST, for each node define a correct range
	function is_ACB (t : in tree) return boolean is
		correct : boolean := true;
		min : key;
		max : key;
	begin
		if is_empty(t) then raise bad_use; end if;

		-- set range of root node
		min := key'first;
		max := key'last;

		is_ACB0(t.root,correct, min, max);
		
		return correct;
	end is_ACB;

end d_binary_tree;
