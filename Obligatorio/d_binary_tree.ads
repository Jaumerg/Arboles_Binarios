with ada.text_io; use ada.text_io;
with dqueue;
with dlist;

generic
	type key is (<>);
package d_binary_tree is

	type tree is limited private;

	package chardlist is new dlist (item => key);
	use chardlist;
	package chardqueue is new dqueue (item => key);
	use chardqueue;

	bad_use : exception;
	space_overflow : exception;

	-- Prepare the free tree to start the operations
	procedure empty (t: out tree);
	pragma inline(empty);

	-- Contruct a tree taking the keys from a file introduced by arguments
	-- the in file must content two lines, first one must be the inorder path
	-- and second line must be the preorder path.
	procedure construct_tree_by_paths (t: in out tree; lin: in out list;
															qpre: in out queue);

	-- Use an inorder path to check if the created tree es correct respect to
	-- file inorder path, writes a 1 if the tree is correct or 0 if is no in a 
	-- file called resultados.txt
	procedure check_tree (t: in tree; lin: in out list; c : in out boolean);
	
	-- Return a boolean depending on the root content
	function is_empty (t : in tree) return boolean;
	pragma inline (is_empty);

	-- Check if the tree introduced by arguments is an Binary Search Tree 
	-- ACB (Arbre de Cerca Binaria)
	-- In this trees for all the nodes, all nodes in left subtree are lesser than
	-- root and all nodes in right subtree are bigger than root
	function is_ACB (t : in tree) return boolean;

private
	type node;
	type pnode is access node;

	type node is
		record 
			k: key;
			lc,rc: pnode;
		end record;

	type tree is
		record
			root: pnode;
		end record;
end d_binary_tree;
