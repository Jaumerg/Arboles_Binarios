with Ada.Command_Line; use Ada.Command_Line;
with Ada.Text_IO; use Ada.Text_IO;
with Ada.Integer_Text_IO; use Ada.Integer_Text_IO;

with d_binary_tree;
with dqueue;

procedure main is

	package chard_binary_tree is new d_binary_tree (key => character);
	use chard_binary_tree;
	use chardqueue;
	use chardlist;

	mytree : tree;
	file : File_Type;
	lin : list; 
	qpre : queue;
	correct : boolean := false;

	-- Contruct two queues with the content of the file introduced by
	-- parameters. The first line in the file correspond to inorder path
	-- and the second lines correspond to preorder path
	procedure init_structures_by_file (lin : out list; qpre : out queue; 
														f: in out file_type) is
		read : character;
	begin

		empty(qpre);
		empty(lin);

		while not End_Of_Line(f) loop
			get(f,read);
			if read /= ' ' then
				insert(lin, read);
			end if;
		end loop;

		while not End_Of_File(f) loop
			get(f, read);
			if read /= ' ' then
				qinsert(qpre,read);
			end if;
		end loop;


	end init_structures_by_file;

	-- Write 1 in a file if the boolean variable introduced by parametrs is true
	-- if is false write a 0 in the file
	procedure put_result_on_file (file: in out file_type; bol : in boolean) is
	begin
		if bol then
			put(file,"1");
		else
			put(file,"0");
		end if;
	end put_result_on_file;

begin

	if Argument_Count < 1 then
		Put_Line("No se ha introducido el nombre del fichero");
	else
		
		open(file, mode => in_file, name => Argument(1));
		init_structures_by_file(lin, qpre, file);
		close(file);

		empty(mytree);

		construct_tree_by_paths(mytree, lin, qpre);

		create(file, mode => out_file, name => "resultados.txt");

		check_tree(mytree,lin,correct);
		put_result_on_file(file, correct);
		
		correct := is_ACB(mytree);
		put_result_on_file(file, correct);

		close(file);
	
	end if;

end main;

