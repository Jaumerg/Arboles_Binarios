package body dqueue is

	-- Prepare the queue to start with the operation
	procedure empty (myqueue : out queue) is
	begin
		myqueue.first := null; myqueue.last := null;
	end empty;

	-- Insert new element on the queue 
	procedure qinsert (myqueue : in out queue; x : in item) is
		newcell : pcell;
	begin
		newcell := new cell;
		newcell.all := (x,null);
		if(myqueue.first = null) then
			myqueue.first := newcell;
			myqueue.last := newcell;
		else
			myqueue.last.next := newcell;
			myqueue.last := newcell;
		end if;
	exception
		when storage_error => raise space_overflow;
	end qinsert;

	-- First element leaves the queue
	procedure rm_first (myqueue : in out queue) is
	begin
		myqueue.first := myqueue.first.next;
		if myqueue.first = null then myqueue.last := null; end if;
	exception 
		when constraint_error => raise bad_use;
	end rm_first;

	-- Get the element at first position on the queue
	function get_first (myqueue : in queue) return item is
	begin
		return myqueue.first.x;
	exception 
		when constraint_error => raise bad_use;
	end get_first;

	function is_empty (myqueue : in queue) return boolean is
	begin
		return myqueue.first = null;
	end is_empty;

end dqueue;
