generic
	type item is (<>);
package dqueue is

	type queue is private;

	-- Exceptions
	space_overflow: exception;
	bad_use: exception;

	-- Inicializate the queue to empty
	procedure empty (myqueue : out queue);
	pragma inline(empty);

	-- Introduce new item in the queue
	procedure qinsert (myqueue : in out queue; x : in item);

	-- Remove the first item in the queue
	procedure rm_first (myqueue : in out queue);

	-- Get the value of the first item in the queue
	function get_first (myqueue : in queue) return item;
	pragma inline(get_first);

	-- Check if the queue has any item
	function is_empty (myqueue : in queue) return boolean;
	pragma inline(is_empty);

private 
	
	type cell;
	type pcell is access cell;

	type cell is
		record
			x : item;
			next : pcell;
		end record;

	type queue is
		record
			first, last : pcell;
		end record;

end dqueue;
